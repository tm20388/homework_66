class CommentsController < ApplicationController

  def new
    @post = Post.find(params[:post_id])
  end

   def create
    @post = Post.find(params[:post_id])
    @comment = @post.comments.create(comment_params)
    @comment.user = current_user
    redirect_to users_path
    # respond_to do |format|
    #   format.html { redirect_to book_show_url(book) }
    #   format.js {}
    # end
  end

  private

  def comment_params
    params.require(:comment).permit(:body)
  end
end