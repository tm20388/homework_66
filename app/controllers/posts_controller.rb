class PostsController < ApplicationController
  def create
    @post = Post.new(post_params)
    @post.user = current_user
    @post.photos.attach(params[:post][:photos])
    if @post.save
     redirect_to user_path(current_user) 
    end
  end

  private

  def post_params
    params.require(:post).permit(:description)
  end
end
