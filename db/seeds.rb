# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
def copy_image_fixture(product, file)
  fixtures_path = Rails.root.join('app', 'assets', 'images', "#{file}.jpg")
  product.avatar.attach(io: File.open(fixtures_path), filename: "#{file}.jpg")
end

user1 = User.create(name: 'Иванов Иван', email: 'ivan@gmail.com', password: '121212',  password_confirmation: '121212')
copy_image_fixture(user1, 1)
user2 = User.create(name: 'Мурат Харашов', email: 'myrat@gmail.com', password: '121212',  password_confirmation: '121212')
copy_image_fixture(user2, 2)
user3 = User.create(name: 'Артемий Хасанов', email: 'ert@gmail.com', password: '121212',  password_confirmation: '121212')
copy_image_fixture(user3, 3)
user4 = User.create(name: 'Марина Трунова', email: 'tm@gmail.com', password: '121212',  password_confirmation: '121212')
copy_image_fixture(user4, 4)
