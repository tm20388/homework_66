Rails.application.routes.draw do
  devise_for :users
  root  "users#index"
  resources :users, :only => [:index, :show]
  resources :posts, :only => [:create] do
    resources :comments
  end
  resources :posts, :only => [:create, :new]

end
